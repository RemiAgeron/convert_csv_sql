<?php
$time_start = microtime(true);

for($i=0; $i<1000000; $i++){

  // ini_set('display_errors', 1);
  // ini_set('display_startup_errors', 1);
  // error_reporting(E_ALL);

  $filename = 'people.csv';     //Le nom du fichier à convertir en base de données

  $servername = 'localhost';    //Le nom du serveur
  $username = 'remi';           //Le nom d'utilisateur
  $password = 'root';           //Le mot de passe de l'utilisateur
  $database = 'simplon_test';   //Le nom de la base de données

  try {
    $conn = new PDO("mysql:host$servername;dbname=$database", $username, $password);
  } catch (PDOException $e) {
    echo 'error' . $e->getMessage();
    die();
  }

  $countries = new ArrayObject();

  $countries->append(array('CS', 'Tchéquie'));
  $countries->append(array('AN', 'Antilles néerlandaises'));

  $res = file_get_contents('https://restcountries.com/v3.1/all');
  $res = json_decode($res);

  foreach($res as $element){

    $code = $element->cca2;
    $name = $element->translations->fra->common;

    $countries->append(array($code, $name));

  }

  $handle = fopen($filename, "r");
  while (($data = fgetcsv($handle)) !== FALSE) {

    if ($data[0] == "id") continue;

    $id = $data[0];

    $first_name = $data[1];

    $last_name = $data[2];

    $mail = filter_var($data[3], FILTER_VALIDATE_EMAIL) ? strtolower($data[3]) : NULL;

    $metier = $data[4];
    if ($metier == "politicien") $metier = "menteur";

    $bd = DATE('Y-m-d', strtotime($data[5]));

    foreach($countries as $entry) {
      if($data[6] == $entry[0]) {
        $country = $entry[1];
        break;
      }
    }

    $tel = "0" . substr($data[7], -9);

    try {

      $sql = $conn->prepare("INSERT INTO simplon_test.people (id, firstname, lastname,email, profession, birthdate,country,phone)
      VALUES (:insert_id,:insert_fn,:insert_ln,:insert_mail,:insert_pro,:insert_bd,:insert_country,:insert_tel)");

      $sql->bindparam('insert_id', $id, PDO::PARAM_INT);
      $sql->bindparam('insert_fn', $first_name, PDO::PARAM_STR);
      $sql->bindparam('insert_ln', $last_name, PDO::PARAM_STR);
      $sql->bindparam('insert_mail', $mail, PDO::PARAM_STR);
      $sql->bindparam('insert_pro', $metier, PDO::PARAM_STR);
      $sql->bindparam('insert_bd', $bd, PDO::PARAM_STR);
      $sql->bindparam('insert_country', $country, PDO::PARAM_STR);
      $sql->bindparam('insert_tel', $tel, PDO::PARAM_STR);

      $sql->execute();

    } catch (PDOException $e) {

      echo 'error' . $e->getMessage();
      die();

    }

  }

  $time_end = microtime(true);
  break;

}

// Je calcule la différence (en seconde)
$execution_time = ($time_end - $time_start);

// J'affiche le résultat
echo "Temps d'execution: ".$execution_time."sec";